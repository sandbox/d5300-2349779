(function($) {
    Drupal.behaviors.myBehavior = {
        attach: function (context, settings) {
            //code starts
            var clicked = 0,
                id1,
                id2,
                link;

            $('input[type="checkbox"]').click(function(){

                if($(this).is(':checked')){
                    clicked++;
                    if(clicked == 2){
                        id2 = $(this).val();
                    } else {
                        id1 = $(this).val();
                    }
                } else {
                    clicked--;
                }

                if(clicked == 2){
                    $('input[type="checkbox"]').each(function(){
                        if ($(this).is(':checked')){

                        } else {
                            $(this).hide();
                        }
                    });

                    link = '/admin/structure/views/revisions/compare/'+id1+'/'+id2;
                    $('#compare-two-views').attr('href', link).text('Compare!');

                } else {
                    if(clicked == 1){
                        $('#compare-two-views').text('Select 1 more view');
                    }
                    $('input[type="checkbox"]').show();
                    $('#footer > a').remove();
                }

            })
            //code ends
        }
    };
})(jQuery);



